package execute;

import bot.BotParser;
import bot.BotStarter;
import bot.OurBot;

/**
 * @author #Bugs
 *	Niciun alt scop decat sa seteze botul folosit si sa ruleze parser-ul.
 *  Metoda de main se afla aici.
 */
public class StartGame {

	public static void main(String[] args)
	{
		BotParser parser;
		//parser = new BotParser(new BotStarter());
		parser = new BotParser(new OurBot());
		parser.run();
	}
}
