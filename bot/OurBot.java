package bot;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

import strategii.*;
import map.Map;
import map.Region;
import map.SuperRegion;
import move.AttackTransferMove;
import move.Move;
import move.PlaceArmiesMove;

/**
 * @author #Bugs Pentru proiect PA 2015. Clasa de bot care se ocupa de alegerea
 *         regiunilor de start, de deploy si de attack.
 */
public class OurBot implements Bot {
	private boolean once;

	// miscarile de attack, acestea sunt determinate la deploy si la attack
	static ArrayList<AttackTransferMove> attackTransferMoves = new ArrayList<AttackTransferMove>();
	static ArrayList<PlaceArmiesMove> placeArmiesMoves = new ArrayList<PlaceArmiesMove>();

	// miscarile de attack, acestea sunt determinate la deploy si la attack
	@Override
	/**
	 *	Determina regiunea de start, va incerca sa aleaga superregiunea cu raportul regiuni/BonusArmate cel
	 *  mai mare si sa evite wastelands.
	 */
	public Region getStartingRegion(BotState state, Long timeOut) {
		// calculeaza distantele dintre toate regiunile de pe harta
		// o singura data
		if (!once) {
			Map fullMap = state.getFullMap();
			Euristici.roy_floyd(fullMap);
			once = true;
		}

		ArrayList<Region> wasteLandsList = state.getWasteLands();
		LinkedList<SuperRegion> superRegionsList = state.getFullMap()
				.getSuperRegions();
		Hashtable<Integer, Integer> marked = new Hashtable<Integer, Integer>();

		// marchez SuperRegiunile care au Wastelands si retin in Hash numarul
		// acestora
		for (Region r : wasteLandsList) {
			SuperRegion sr = r.getSuperRegion();
			if (superRegionsList.contains(sr)) {
				if (!marked.containsKey(sr.getId())) {
					marked.put(sr.getId(), 1);
					continue;
				}
				int nrWastelands = marked.get(sr.getId());
				marked.put(sr.getId(), ++nrWastelands);
			} else
				marked.put(sr.getId(), 0);
		}

		/**
		 * ce am facut: am evitat sa aleg superregiunile care au wastelands am
		 * ales regiunea a carei superregiune are cel mare mare raport (reward /
		 * nr de subregiuni)
		 */

		Region startingRegion = null;
		double report = Integer.MIN_VALUE;
		int minNrSubregiuni = Integer.MAX_VALUE;
		ArrayList<Region> pickableRegions = state.getPickableStartingRegions();

		int i = 0;
		while (true) {
			for (Region r : pickableRegions) {
				SuperRegion sr = r.getSuperRegion();
				if (marked.containsKey(sr.getId())
						&& marked.get(sr.getId()) > i)
					continue;
				double superRegionReport = sr.getRaport();
				// cazul in care raportul este acelasi, aleg regiunea a carei
				// superRegiuni are nr cel mai mic de regiuni
				if (superRegionReport == report) { // rapoartele sunt egale
					if (sr.getNumberSubRegions() < minNrSubregiuni) {
						// verific daca super-regiunea are mai putine subregiuni
						// decat nr anterior
						minNrSubregiuni = sr.getNumberSubRegions();
						startingRegion = r;
					}
				} else if (superRegionReport > report) {
					report = superRegionReport; // salvez cel mai mare raport
					startingRegion = r; // aleg regiunea de inceput
					minNrSubregiuni = sr.getNumberSubRegions();
					// marchez numarul de regiuni al super-regiunii respective
				}
			}
			if (startingRegion != null)
				break;
			else
				++i;
		}
		return startingRegion;
	}

	/**
	 * Euristici pentru atacarea inamicului si deploy eficient atunci cand ne
	 * aflam langa inamic
	 * 
	 * @param state
	 *            starea jocului
	 * @param myName
	 *            numele meu ca jucator
	 * @param armiesLeft
	 *            armate ramase pentru deploy
	 * @return armatele care mai raman dupa ce ne-am asigurat regiunile
	 */
	public int atacaInamic(BotState state, String myName, int armiesLeft) {
		LinkedList<Region> myRegions = state.getVisibleMap().getMyRegions();
		// coada de prioritati ce contine 2 regiuni, prima e a noastra, a doua
		// este cea a adversarului
		Queue<Pair> Q = new PriorityQueue<Pair>();
		for (Region r : myRegions) {
			// daca avem adversarul langa noi
			if (r.hasEnemies(state.getOpponentPlayerName())) {
				for (Region n : r.getNeighbors()) {
					if (n.ownedByPlayer(state.getOpponentPlayerName())) {
						// adaugam in coada regiunea noastra si cea a inamicului
						Q.offer(new Pair(r, n));
					}
				}
			}
		}
		System.err.println("Printing queue.");
		// incep parcurgerea cozii
		while (!Q.isEmpty() && armiesLeft > 0) {
			Pair p = Q.poll();
			Region a = p.a; // regiunea mea
			Region b = p.b; // regiunea adversarului
			System.err.println("(" + a.getId() + ", " + b.getId() + ")");
			// daca nu am trecut de runda 5, preferam sa nu incercam formarea
			// unei superregiuni cu waste land pentru ca e costisitoare
			if (state.getPlayerTotalBonus() < 10) {
				boolean hasWasteland = false;
				for (Region r : b.getSuperRegion().getSubRegions()) {
					if (r.ownedByPlayer(state.getMyPlayerName())
							|| r.ownedByPlayer(state.getOpponentPlayerName()))
						continue;
					if (r.getArmies() > 2) {
						hasWasteland = true;
						break;
					}
				}
				if (hasWasteland) {
					System.err.format(
							"Nu atac din %d pentru ca %d are wasteland\n",
							a.getId(), b.getId());
					continue;
				}
				for (Region r : a.getSuperRegion().getSubRegions()) {
					if (r.ownedByPlayer(state.getMyPlayerName())
							|| r.ownedByPlayer(state.getOpponentPlayerName()))
						continue;
					if (r.getArmies() > 2) {
						hasWasteland = true;
						break;
					}
				}
				if (hasWasteland) {
					System.err.format(
							"Nu atac din %d pentru ca %d are wasteland\n",
							a.getId(), b.getId());
					continue;
				}
			}
			// parcurg vecinii mei
			for (Region neigh : a.getNeighbors()) {
				// daca am vecini regiuni ale mele, verific ca ele la randu lor
				// sa nu aiba vecini inamici
				if (neigh.ownedByPlayer(state.getMyPlayerName())
						&& !neigh.hasEnemies(state.getOpponentPlayerName())) {
					// daca am doar o armata nu am ce sa fac
					if (neigh.getArmies() == 1)
						continue;
					// altfel, fac transfer la regiunea mea care e langa inamic
					attackTransferMoves.add(new AttackTransferMove(myName,
							neigh, a, neigh.getArmies() - 1));
					// actualizare armate in regiunea actuala din coada
					a.setArmies(a.getArmies() + neigh.getArmies() - 1);
					// setez armatele cu o unitate, deoarece transfer tot
					neigh.setArmies(1);
				}
			}

			int n = a.getArmies();
			int m = b.getArmies();
			System.err.println("n = " + n + ", m = " + m);

			// euristica dupa diferenta dintre armatele mele si armatele
			// inamicului
			// daca inamicul are mai multe armate, prefer sa ma apar
			if (n < m) {
				double pus = 0.6
						* (double) (b.getArmies() - 1 + state
								.getEnemyTotalBonus()) - a.getArmies() + 1;
				int dePus = (int) Math.round(pus);
				System.err.println("Pun " + dePus + " armate ca sa apar");
				// verific sa mai am armate cu care sa fac deploy
				if (dePus <= armiesLeft && dePus > 0) {
					armiesLeft -= dePus;
					System.err.println("armiesleft= " + armiesLeft);
					placeArmiesMoves.add(new PlaceArmiesMove(myName, a, dePus));
					a.setArmies(a.getArmies() + dePus); // actualizez numarul de
														// armate dupa ce fac
														// deploy
					a.setArmies(1); 
				}
			} else {
				// daca am mai multe armate, prefer sa atac
				double pus = (double) (a.getArmies() + state
						.getEnemyTotalBonus()) / 0.6 - b.getArmies() + 1;
				int dePus = (int) (Math.round(b.getArmies() * 1.6
						- a.getArmies() + 2));
				if (dePus == 1)
					++dePus;
				System.err.println("Pun " + dePus + " armate ca sa atac.");
				// nu am suficiente armate pentru a face deploy
				if (dePus > armiesLeft)
					continue;
				// am destule armate cu care sa atac
				if (dePus < 0) {
					System.err.println("Am destule armate atac.");
					int deAtacat = a.getArmies() - 1;
					int impartire = (Q.size() > 0) ? Q.size() : 1;
					if (deAtacat > Euristici.calculateMinimumForce(
							b.getArmies(), state.getEnemyTotalBonus()
									/ impartire))
						;
					deAtacat = Euristici.calculateMinimumForce(b.getArmies(),
							state.getEnemyTotalBonus()) / impartire + 1;
					// atac inamicul
					attackTransferMoves.add(new AttackTransferMove(myName, a,
							b, deAtacat));
					a.setArmies(a.getArmies() - deAtacat);
				} else {
					// fac deploy dupa care atac inamicul
					armiesLeft -= dePus;
					System.err.println("armiesleft= " + armiesLeft);
					if (a.getArmies() + dePus - 1 < 2)
						continue;
					placeArmiesMoves.add(new PlaceArmiesMove(myName, a, dePus));
					attackTransferMoves.add(new AttackTransferMove(myName, a,
							b, a.getArmies() + dePus - 1));
					a.setArmies(1);
				}
			}
		}

		System.err.println("armiesleft after enemies= " + armiesLeft);
		return armiesLeft;
	}

	@Override
	/*
	 * Determina deploy-urile pentru runda. Va analiza posibilitati de atac
	 * pentru a realiza o distributie cat mai optima a armatelor. Prioritate vor
	 * avea atacurile pentru a cuceri superregiuni, apoi cele pentru inamic iar
	 * in final se va face o distributie pentru a cuceri cat mai multe regiuni
	 * cu cat mai putine unitati. La final, unitatile ramase vor fi puse fie
	 * langa o regiune care e marginita de cea mai slaba regiune a adversarului,
	 * fie va realiza o distributie uniforma pe front.
	 * 
	 * @return The list of PlaceArmiesMoves for one round
	 */
	public ArrayList<PlaceArmiesMove> getPlaceArmiesMoves(BotState state,
			Long timeOut) {
		attackTransferMoves.clear();
		placeArmiesMoves.clear();

		String myName = state.getMyPlayerName();
		int armiesLeft = state.getStartingArmies();
		LinkedList<Region> myRegions = state.getVisibleMap().getMyRegions();

		System.err.println("Tura " + state.getRoundNumber());
		System.err.println("Bonusul meu: " + state.getPlayerTotalBonus());
		System.err.println("Bonusul lui: " + state.getEnemyTotalBonus());

		// cautam miscari de cucerit superregiuni intr-o singura tura
		armiesLeft = cuceresteSuperregiuni(state, myName, armiesLeft);

		// vedem care regiuni sunt vecine cu inamicul
		armiesLeft = atacaInamic(state, myName, armiesLeft);

		// cauta o metoda optima de a face cat mai multe atacuri cu armatele pe
		// care le avem.
		armiesLeft = distribuieOptimDeploy(state, myName, armiesLeft, myRegions);

		// mai sunt armate ramase, le distribuim egal pe front.
		while (armiesLeft > 0) {
			for (Region r : state.getVisibleMap().getMyRegions())
				if (r.ownedByPlayer(myName) && r.isOnFrontier()) {
					placeArmiesMoves.add(new PlaceArmiesMove(myName, r, 1));
					r.setArmies(r.getArmies() + 1);
					armiesLeft--;
					if (armiesLeft == 0)
						break;
				}
		}

		for (PlaceArmiesMove m : placeArmiesMoves) {
			Region r = m.getRegion();
			for (AttackTransferMove m2 : attackTransferMoves) {
				if (m2.getFromRegion() != null && !m2.isSuperRegionTake()
						&& !m2.isDeOptim())
					m2.setArmies(m2.getArmies() + r.getArmies() - 1);
			}
		}
		return placeArmiesMoves;
	}

	@Override
	/**
	 * Realizeaza atacuri versus regiuni neutre daca 
	 * @return Lista de PlaceArmiesMoves pentru runda respectiva
	 */
	public ArrayList<AttackTransferMove> getAttackTransferMoves(BotState state,
			Long timeOut) {

		String myName = state.getMyPlayerName();

		LinkedList<Region> myRegions = state.getVisibleMap().getMyRegions();

		// in BotState linia 204 initializez toate regiunile pt isAttacked =
		// false, region.setAttacked(false);
		for (Region r : state.getVisibleMap().getRegions())
			r.setAttacked(false);

		// parcurg toate regiunile mele
		for (Region r : myRegions) {
			if (!r.isOnFrontier()) {
				// regiunea este inconjurata de regiuni aliate, nu are potential
				// de atac.
				// daca are mai mult de o armata acestea vor fi trimise la cea
				// mai apropriata regiune de pe
				// front.
				if (r.getArmies() == 1)
					continue; // o armata
				// transfer
				int transferId = Euristici.getTransferRegionId(myRegions,
						r.getId());
				Region transfer = state.getFullMap().getRegion(transferId);
				// transfer la regiunea aleasa
				attackTransferMoves.add(new AttackTransferMove(myName, r,
						transfer, r.getArmies() - 1));
			}

			if (r.getArmies() == 1)
				continue; // am 1 unitate armata, nu pot ataca

			// daca am trupe > 1
			if (r.getArmies() > 1) {
				int armies = r.getArmies() - 1;

				int nrInamici = 0;
				// numarul de regiuni inamice care marginesc regiunea r
				for (Region enemy : r.getNeighbors()) {
					if (enemy.ownedByPlayer(state.getOpponentPlayerName())) {
						nrInamici++;
					}
				}

				// parcurg regiunile vecine
				for (Region enemy : r.getNeighbors()) {
					if (enemy.ownedByPlayer(myName))
						continue;

					if (!enemy.ownedByPlayer(state.getOpponentPlayerName())
							&& nrInamici != 0) {
						continue;
					}

					if (r.isOnFrontier && nrInamici != 0
							&& (r.getArmies() > 1.4 * enemy.getArmies())) {
						attackTransferMoves.add(new AttackTransferMove(myName,
								r, enemy, armies));
						// setez Attacked = true
						// enemy.setAttacked(true);
						armies -= armies;
						break;
					}

					int attackForce, potential = 0;
					attackForce = Euristici.calculateMinimumForce(
							enemy.getArmies(), potential);
					// aici am adaugat conditia ca isAttacked = true
					if (armies >= attackForce && !enemy.isAttacked()) {
						attackTransferMoves.add(new AttackTransferMove(myName,
								r, enemy, attackForce));
						// setez Attacked = true
						enemy.setAttacked(true);
						armies -= attackForce;
					}

				}
			}
		}
		return attackTransferMoves;
	}

	/**
	 * @param state
	 *            starea jocului
	 * @param myName
	 *            numele meu ca jucator
	 * @param armiesLeft
	 *            numarul de armate ramase de pus
	 * @param myRegions
	 *            lista cu regiunile mele
	 * @return numarul de armate ramase dupa distributie.
	 *
	 *         Realizeaza o distributie optima de deploy-uri pentru a garanta
	 *         cucerirea a cat mai multe regiuni.
	 */
	private int distribuieOptimDeploy(BotState state, String myName,
			int armiesLeft, LinkedList<Region> myRegions) {
		// miscari "candidat"
		PriorityQueue<Candidate> candidati = new PriorityQueue<Candidate>();

		// regiunea vecina celui mai vulnerabil inamic, armatele ramase dupa
		// distributie vor fi puse
		// aici
		Region attackWeakestEnemy = null;
		int minEnemy = 99999;
		for (Region r : myRegions)
			if (r.isOnFrontier()) {
				// regiune pe frontiera, are sens sa cautam atacuri
				PriorityQueue<Candidate> maximLocal = new PriorityQueue();
				for (Region enemy : r.getNeighbors()) {
					if (enemy.ownedByPlayer(myName))
						continue;
					int armateInamice = enemy.getArmies(), potential = 0;
					if (enemy.ownedByPlayer(state.getOpponentPlayerName())) {
						// regiune inamica, verificam daca este cea mai slaba
						if (enemy.getArmies() < minEnemy) {
							minEnemy = enemy.getArmies();
							attackWeakestEnemy = r;
						}
						continue;
					}
					// numarul de armate necesare pentru a garanta cucerirea
					int force = Euristici.calculateMinimumForce(armateInamice,
							potential);
					int deAdaugat = force + 1 - r.getArmies();
					// o posibila optiune de deploy
					maximLocal.add(new Candidate(r, deAdaugat, force, enemy,
							state));
				}
				int costSuplimentar = 0;
				LinkedList<Candidate> addToQ = new LinkedList<Candidate>();
				r.setActualForce(r.getArmies());
				for (Candidate c : maximLocal) {
					// daca o regiune este deja folosita pentru atac, atunci ea
					// nu mai va avea
					// forte de atac in afara celor puse de catre deploy
					if (c.getDeAdaugat() > armiesLeft)
						continue;
					if (c.getToAttack().ownedByPlayer(
							state.getOpponentPlayerName())) {
						continue;
					}
					// forta efectiva pentru un atac
					int forta = Euristici.calculateMinimumForce(c.getToAttack()
							.getArmies(), 0);
					int nouDeAdaugat = forta + 1 - r.getActualForce();
					if (nouDeAdaugat < 0)
						nouDeAdaugat = 0;
					if (nouDeAdaugat > armiesLeft)
						continue;
					System.err
							.format("Optim candidat: %d -> %d dePus:%d atacCu:%d ramanCu:%d\n",
									r.getId(), c.getToAttack().getId(),
									nouDeAdaugat, forta, r.getActualForce()
											+ nouDeAdaugat - forta);

					c.deAdaugat(nouDeAdaugat);
					r.setActualForce(r.getActualForce() + c.getDeAdaugat()
							- forta);
					c.getToAttack().setCandidate(true);
					addToQ.add(c);

					// candidatul de deploy c: caracterizat de regiunea pe care
					// se va face deploy,
					// numarul de armate de pus, numarul de armate necesare
					// pentru a cuceri si regiunea
					// pe care o tinteste
				}
				System.err.println("Printez candidati pentru " + r.getId());
				for (Candidate c : addToQ) {
					System.err.format("Deploy %d -> %d   pun:%d  tinta:%d\n",
							r.getId(), c.getToAttack().getId(),
							c.getDeAdaugat(), c.getTarget());
				}
				candidati.addAll(addToQ);
			}
		System.err.println("EXECUT MISCARI DE OPTIM");

		Candidate c;
		while ((c = candidati.poll()) != null) {
			System.err.format("In coada: %d -> %d\n", c.getRepresentative()
					.getId(), c.getToAttack().getId());
			if (armiesLeft == 0)
				break;
			if (c.getToAttack().isAttacked())
				continue;
			Region r = c.getRepresentative();
			// daca este nevoie de mai multe armate decat au ramas
			if (c.getDeAdaugat() > armiesLeft)
				continue;
			// daca deja are forta suficienta
			System.err
					.format("Regiunea %d -> %d   armate:%d deAdaugat:%d ramas:%d  target=%d\n",
							r.getId(), c.getToAttack().getId(), c.getTarget(),
							c.getDeAdaugat(), r.getArmies() + c.getDeAdaugat()
									- c.getTarget(), c.getTarget());
			// vom alege miscarea respectiva
			if (c.getDeAdaugat() < 1)
				c.setDeAdaugat(0);
			else
				placeArmiesMoves.add(new PlaceArmiesMove(myName, r, c
						.getDeAdaugat()));
			AttackTransferMove m = new AttackTransferMove(myName, r,
					c.getToAttack(), c.getTarget());
			m.setDeOptim(true);
			c.getToAttack().setAttacked(true);
			attackTransferMoves.add(m);
			// actualizam numarul de armate
			r.setArmies(r.getArmies() + c.getDeAdaugat() - c.getTarget());
			armiesLeft -= c.getDeAdaugat();
			// regiunea de atacat este flagged
			c.getToAttack().setAttacked(true);
			if (armiesLeft == 0)
				break;
		}
		// pun ce mai ramane langa cel mai slab inamic
		if (attackWeakestEnemy != null && armiesLeft > 0) {
			placeArmiesMoves.add(new PlaceArmiesMove(myName,
					attackWeakestEnemy, armiesLeft));
			attackWeakestEnemy.setArmies(attackWeakestEnemy.getArmies()
					+ armiesLeft);
			armiesLeft = 0;
		}
		return armiesLeft;
	}

	/**
	 * @param state
	 *            starea jocului
	 * @param myName
	 *            numele jucatorului meu
	 * @param armiesLeft
	 *            numarul de armate ramase de pus in aceasta tura
	 * @return numarul de armate ramase dupa parcurgerea si atacarea
	 *         superregiunilor.
	 * 
	 *         Cauta sa cucereasca superregiuni in tura aceasta evitand pe cele
	 *         care au regiuni inamice in interior/vecin sau care contin
	 *         wastelands
	 */
	private int cuceresteSuperregiuni(BotState state, String myName,
			int armiesLeft) {
		Queue<SGTakePair> Q = new PriorityQueue<SGTakePair>();
		for (SuperRegion sg : state.getVisibleMap().getSuperRegions()) {
			if (sg.getArmiesReward() == 0)
				continue;
			if (!sg.ownedByPlayer().equals(""))
				continue;
			// parcurgem superregiunile vizibile
			LinkedList<Candidate> miscari = Euristici
					.takeSuperregion(state, sg);
			// nu este posibil sa cucerim aceasta superregiune
			if (miscari == null)
				continue;
			int totalCost = 0;
			// determin costul total necesar cuceririi superregiunii
			boolean hasWasteland = false;
			for (Candidate c : miscari) {
				totalCost += c.getDeAdaugat();
				if (!c.getToAttack().ownedByPlayer(state.getMyPlayerName())
						&& c.getToAttack().getArmies() > 2)
					hasWasteland = true;
			}
			// if (hasWasteland && state.getPlayerTotalBonus() < 13)
			// continue;
			Q.offer(new SGTakePair(sg, totalCost, miscari));
		}
		while (!Q.isEmpty()) {
			SGTakePair p = Q.poll();
			SuperRegion sg = p.getSg();
			int totalCost = p.getCost();
			LinkedList<Candidate> miscari = p.getMiscari();
			System.err.println("Pot cuceri sg: " + sg.getId() + "  cu costul: "
					+ totalCost);
			// daca costul este mai mare decat numarul de armate ramase, nu pot
			// cuceri
			if (totalCost > armiesLeft)
				continue;
			armiesLeft -= totalCost;

			// execut fiecare miscare necesara cuceririi superregiunii
			System.err.format(
					"Execut miscari pentru a cuceri superregiunea: %d\n",
					sg.getId());
			for (Candidate c : miscari) {
				if (c.getDeAdaugat() > 0)
					placeArmiesMoves.add(new PlaceArmiesMove(myName, c
							.getRepresentative(), c.getDeAdaugat()));
				AttackTransferMove m = new AttackTransferMove(myName,
						c.getRepresentative(), c.getToAttack(), c.getTarget());
				m.setSuperRegionTake(true);
				c.getToAttack().setAttacked(true);
				attackTransferMoves.add(m);

				System.err
						.format("Atac: regiunea %d -> %d    armate:%d ramas:%d\n",
								c.getRepresentative().getId(),
								c.getToAttack().getId(),
								c.getTarget(),
								c.getRepresentative().getArmies()
										+ c.getDeAdaugat() - c.getTarget());
				c.getRepresentative().setArmies(
						c.getRepresentative().getArmies() + c.getDeAdaugat()
								- c.getTarget());
			}
		}
		return armiesLeft;
	}
}
