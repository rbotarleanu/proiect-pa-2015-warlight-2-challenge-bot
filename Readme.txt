Echipa: #Bugs
Seria: CB
Botarleanu Robert-Mihai  capitan
Florea Anda-Madalina
Cioaca Valentin-Sergiu
Mititiuc Catalin

-----------------------------------------
Cuprins
1. Strategie generala, euristici
2. Pick - alegerea regiunilor
3. Deploy - punerea armatelor
4. Attack & Transfer
5. Modificari facute la starterbot
6. Probleme intampinate
7. Referinte
-----------------------------------------
1. Strategie generala, euristici
   Am decis ca metoda cea mai buna de a manevra pe harta este sa cucerim superregiuni intr-un mod cat mai rapid 
si folosind numarul minim de armate. Algoritmul nostru va verifica la fiecare pas daca este posibila cucerirea 
unei superregiuni intr-o singura runda(aceasta verificare fiind realizata inaintea fiecarei runde) si va 
distribui armate/atacuri pentru a face acest lucru posibil. Alegerea regiunilor de start este facuta intr-un mod
care sa ajute in acest scop.
   Euristia principala folosita este urmatoarea : calculam forta minima pe care trebuie sa o aiba botul nostru 
pentru a putea ataca adversarul si luand in calcul armatele pe care acesta le avea la  sfarsitul rundei 
precedente sa fim siguri ca ar putea castiga daca are  loc atacul.
   Formula este Math.round((armies + potential) / 0.6).De aceea noi la deploy punem armate astfel incat sa acoperim 
diferenta dintre forta minima necesara +1  si armatele pe care le aveam inainte de deploy.
   Cand vine vorba de atacatul regiunilor neutre, se incearca alegerea unor atacuri care sa minimizeze numarul de 
resurse si de ture necesare cuceririi unei superregiuni. Cand vine vorba de inamic, se va incerca actionarea pe
regiunile care au o diferenta de forta cat mai benefica, pentru a maximiza castigurile posibile.

   
2. Pick - alegerea regiunilor
	Alegerile initale ale regiunilor s-au facut pe baza raportului dintre bonusul superregiunii din care face 
parte si numarul de regiuni. Daca avem de ales intre mai multe regiuni care fac parte din superregiuni ce au 
acelasi raport, vom alegea acea regiune ca carei supraregiuni are un numar mai redus de regiuni. Incercam pe cat 
posibil sa ne ferim de wasteland-uri, dar daca doar ele mai raman, alegerile sunt facute pe baza acelorasi 
criterii. 

3. Deploy
	Avand in vedere stransa legatura dintre deploy si attack, acestea s-au realizat simultan.
	Punctul central al strategiei noastre este cucerirea superregiunilor, in acest sens, in interiorul clasei
Euristici exista clasa statica "takeSuperregion" care analizeaza o superregiune daca ea poate fi cucerita in
aceasta tura. Daca superregiunea respectiva contine o regiune inamica sau este invecinata de o sueprregiune
inamica atunci se considera ca nu are sens aceasta operatie.
	Functia distribuieOptimDeploy va alege miscarile cele mai benefice de cucerire a regiunilor neutre. Prin-
cipalul criteriu dupa care se face aceasta alegere este numarul de miscari necesare cuceririi unei superregiuni,
tinand cont de numarul de regiuni pe care deja le detinem in supraregiunea respectiva, de numarul de armate nece-
sare cuceririi si de bonusul pe care il aduce superregiunea.	
	Dupa cucerirea superregiunilor si distributia in mod optim a deploy-urilor, se poate sa mai ramana armate
de plasat. Acestea se vor pune pe o regiune care se afla vecina cu cea mai slaba regiune vizibila a inamicului.
In acest fel, se asigura ca vom putea obtine un avangaj in acea zona.

4. Attack & Transfer
4.1 Attack
	Dupa cum s-a mentionat la sectiunea Deploy, prioritate reprezinta cucerirea superregiunilor, acestea se vor
realiza primele. 
	Folosind formula descrisa la sectiunea (1)-Euristici, se vor determina atacurile care pot fi realizate
din fiecare regiune pentru a cuceri regiunile adiacente folosind numarul minim de unitati. Acest lucru este
aplicabil doar pentru regiunile neutre, pentru regiunile inamicului va trebui facuta o analiza diferita.
	Deoarece inamicul actioneaza intr-un fel imprevizibil, am decis ca atunci cand suntem vecini cu el sa 
incercam sa il atacam cand avem cel putin 1.4*numarul de armate ale lui, pentru a crea o diferenta cat mai
buna pentru noi intre armatele pe care le are el si pe care le avem noi. Deoarece algoritmul nostru se bazeaza
pe cucerirea superregiunilor, daca putem opri inamicul sa isi stranga forte prea mari in zone locale, avem
posibilitatea sa il intrecem cu un numar mai mare de armate de pus pe tura.


4.2 Transfer
	Am ales ca transferul sa se realizeze de la regiunile interioare(care nu au vecini neutrii/inamici) catre 
regiunile de pe front(cu cel putin o regiune nealiata ca vecin). In acest sens, o singura data in faza de alegere
a regiunilor de start am aplicat algoritmul Roy-Floyd pentru a determina distantele minime dintre toate regiunile
de pe harta. Acestea sunt retinute, impreuna cu o matrice de ID-uri de regiuni are reprezinta regiunea urmatoare
pe o cale de drum minim, in interiorul clasei Euristici ca niste variabile statice.
	Astfel, cand se gaseste in timpul etapei de Attack o regiune interna cu mai mult de o armata, celelalte
unitati vor fi trimise catre regiunea urmatoare de pe drumul catre cea mai apropriata regiune de pe front.

5. Modificari facute la starterbot
   Clasa BotState
   In metoda "updateMap", se actualizeaza in Map regiunile proprii si cele ale inamicului. 
   
   Clasa Map 
   Adaugate listele de Region: myRegions, enemyRegions si getteri/setteri aferenti.

   Clasa Region
   Adaugate atributele: isAttacked, isOnFrontier, actualForce si getteri/setteri aferenti.
	Acestea sunt, pe rand: un flag pentru a indica faptul ca regiunea respectiva este atacata deja de o regiune
a noastra, isOnFrontier - un flag care indica faptul ca regiunea are cel putin o regiune nealiata ca vecin, 
actualForce - forta efectiva pe care o poate folosi pentru a ataca(diferita de armies daca regiunea a fost deja
folosita intr-o tura pentru un atac).

6. Probleme intampinate
	Dificultati cu starterbot-ul oferit, le-am rezolvat prin modificarea codului sursa pentru a face mai usoara
programarea logicii jocului.
	Decizia unei strategii de atac, deoarece aceasta este bazata pe euristici a fost necesara experienta cu jocul
Warlight prin meciuri jucate de noi individual, dar si de urmarirea meciurilor celor mai buni boti de pe 
theaigames.com (RealityBomb, LuckyBot, GreenTea).

7. Referinte
	Am preluat idei din urmatoarele surse:
http://www.bluemoonproductions.nl/warlight-bot-tactics/   
https://github.com/Norman1/SupremeDalek/tree/master/src   (ideea de atac a superregiunii intr-o tura)

	Am urmarit meciuri pentru botii RealityBomb ,LuckyBot:
http://theaigames.com/competitions/warlight-ai-challenge-2/game-log/RealityBomb/1
http://theaigames.com/competitions/warlight-ai-challenge-2/game-log/LuckyBot/1

	

