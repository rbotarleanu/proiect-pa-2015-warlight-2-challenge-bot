/**
 * Warlight AI Game Bot
 *
 * Last update: January 29, 2015
 *
 * @author Jim van Eeden
 * @version 1.1
 * @License MIT License (http://opensource.org/Licenses/MIT)
 */

package map;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;

import move.Move;


public class Region implements Comparable {
	
	private int id;
	private LinkedList<Region> neighbors;
	private SuperRegion superRegion;
	private int armies;
	private String playerName;
	
	private boolean isCandidate;
	private boolean isAttacked;
	public boolean isOnFrontier;
	private int actualForce;
	
	
	public Region(int id, SuperRegion superRegion)
	{
		this.id = id;
		this.superRegion = superRegion;
		this.neighbors = new LinkedList<Region>();
		this.playerName = "unknown";
		this.armies = 0;
		this.actualForce = 0;
		superRegion.addSubRegion(this);
	}
	
	public Region(int id, SuperRegion superRegion, String playerName, int armies)
	{
		this.id = id;
		this.superRegion = superRegion;
		this.neighbors = new LinkedList<Region>();
		this.playerName = playerName;
		this.armies = armies;
		this.actualForce = armies;
		
		superRegion.addSubRegion(this);
	}
	
	public void addNeighbor(Region neighbor)
	{
		if(!neighbors.contains(neighbor))
		{
			neighbors.add(neighbor);
			neighbor.addNeighbor(this);
		}
	}
	
	public int attackingArmies() {
		String myName = this.getPlayerName();
		int armies = 0;
		for(Region r: neighbors) {
			if(r.getPlayerName().equals("neutral")) continue;
			if(r.getPlayerName().equals(myName)) continue;
			armies += r.armies;
		}
		return armies;
	}
	
	/**
	 * @param region a Region object
	 * @return True if this Region is a neighbor of given Region, false otherwise
	 */
	public boolean isNeighbor(Region region)
	{
		if(neighbors.contains(region))
			return true;
		return false;
	}

	/**
	 * @param playerName A string with a player's name
	 * @return True if this region is owned by given playerName, false otherwise
	 */
	public boolean ownedByPlayer(String playerName)
	{
		if(playerName.equals(this.playerName))
			return true;
		return false;
	}
	
	/**
	 * @param armies Sets the number of armies that are on this Region
	 */
	public void setArmies(int armies) {
		this.armies = armies;
	}
	
	/**
	 * @param playerName Sets the Name of the player that this Region belongs to
	 */
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	/**
	 * @return The id of this Region
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @return A list of this Region's neighboring Regions
	 */
	public LinkedList<Region> getNeighbors() {
		return neighbors;
	}
	
	/**
	 * @return The SuperRegion this Region is part of
	 */
	public SuperRegion getSuperRegion() {
		return superRegion;
	}
	
	/**
	 * @return The number of armies on this region
	 */
	public int getArmies() {
		return armies;
	}
	
	/**
	 * @return A string with the name of the player that owns this region
	 * unknown - regiune din fog
	 * neutral - neutra
	 * getEnemyPlayerName daca vrei sa vezi ca e inamica(nu lasati player2 sau ceva)
	*/


	public String getPlayerName() {
			return playerName;
	}

	public boolean isAttacked() {
		return isAttacked;
	}

	public void setAttacked(boolean isAttacked) {
		this.isAttacked = isAttacked;
	}

	public boolean isOnFrontier() {
		return isOnFrontier;
	}

	public void setOnFrontier(boolean isOnFrontier) {
		this.isOnFrontier = isOnFrontier;
	}

	public int getActualForce() {
		return actualForce;
	}

	public void setActualForce(int actualForce) {
		this.actualForce = actualForce;
	}
	public boolean isNeighbour(Region r){
		return neighbors.contains(r);
	}

	/**
	 * @param opponent numele inmaicului
	 * @return true daca regiuni inamice ca vecini
	 */
	public boolean hasEnemies(String opponent) {
		// TODO Auto-generated method stub
		for(Region r: neighbors) {
			if(r.ownedByPlayer(opponent)) return true;
		}
		return false;
	}

	@Override
	public int compareTo(Object arg0) {
		Region other = (Region)arg0;
		boolean fullSg1 = !this.getSuperRegion().ownedByPlayer().equals("");
		boolean fullSg2 = !other.getSuperRegion().ownedByPlayer().equals("");
		if(fullSg1 == true && fullSg2 == false) {
			return -1;
		}
		if(fullSg1 == false && fullSg2 == true) {
			return 1;
		}
		return (this.attackingArmies() - other.attackingArmies());
	}

	public boolean isCandidate() {
		return isCandidate;
	}

	public void setCandidate(boolean isCandidate) {
		this.isCandidate = isCandidate;
	}
}
