package strategii;

import bot.BotState;
import map.Region;

/**
 * @author #Bugs Clasa candidat pentru o miscare de deploy/attack.
 */
public class Candidate implements Comparable<Candidate> {

	// O clasa ce reprezinta un candidat pentru o miscare de deploy
	// are compareTo si e bun de folosit cu un PriorityQueue pentru a alege
	// miscarile cele mai optime

	private Region representative;
	private Region toAttack;
	private int deAdaugat;
	private int target;
	private BotState state;

	public Candidate(Region representative, int force, int target,
			Region toAttack, BotState state) {
		super();
		this.representative = representative;
		this.deAdaugat = force;
		this.target = target;
		this.toAttack = toAttack;
		this.state = state;
	}

	@Override
	public int compareTo(Candidate o) {
		//se calculeaza mai intai numarul de armate necesare cuceririi superregiunii
		int dePusDiff = o.deAdaugat - deAdaugat;
		int deAtacatToTake1 = 0;
		int deAtacatToTake2 = 0;

		boolean enemy1 = false, enemy2 = false;
		int alliedRegions1 = 0, alliedRegions2 = 0;
		//daca una din optiuni este atacul unui wasteland, va avea prioritate mai mica
		if (toAttack.getArmies() > 2 && o.toAttack.getArmies() < 3)
			return 1;
		if (toAttack.getArmies() < 3 && o.toAttack.getArmies() > 2)
			return -1;
		//daca este vorba de o superregiune care nu are bonus de armate, are prioritate
		//mai mica
		if (toAttack.getSuperRegion().getArmiesReward() == 0
				&& !(o.toAttack.getSuperRegion().getArmiesReward() == 0))
			return 1;
		if (!(toAttack.getSuperRegion().getArmiesReward() == 0)
				&& (o.toAttack.getSuperRegion().getArmiesReward() == 0))
			return -1;
		//speculez numarul de armate necesare, numarul de regiuni aliate si daca 
		//exista un inamic in superregiunea respectiva
		for (Region r : o.toAttack.getSuperRegion().getSubRegions()) {
			if (r.getPlayerName().equals(state.getMyPlayerName())) {
				++alliedRegions1;
				continue;
			}
			if (r.getPlayerName().equals(state.getOpponentPlayerName()))
				enemy1 = true;
			if (r.getArmies() > 2) {
				System.err.format("Atac din %d -> %d gasit wasteland:%d\n", o
						.getRepresentative().getId(), o.toAttack.getId(), r
						.getId());
				deAtacatToTake1 += 11;
			} else
				deAtacatToTake1 += 3;
		}
		//la fel si pentru celalat candidat
		for (Region r : toAttack.getSuperRegion().getSubRegions()) {
			if (r.getPlayerName().equals(state.getMyPlayerName())) {
				++alliedRegions2;
				continue;
			}
			if (r.getPlayerName().equals(state.getOpponentPlayerName()))
				enemy2 = true;
			if (r.getArmies() > 2) {
				System.err.format("Atac din %d -> %d gasit wasteland:%d\n",
						getRepresentative().getId(), toAttack.getId(),
						r.getId());
				deAtacatToTake2 += 11;
			} else
				deAtacatToTake2 += 3;
		}
		System.err.format("Compar: (%d->%d) cu (%d->%d)\n",
				representative.getId(), toAttack.getId(),
				o.representative.getId(), o.toAttack.getId());
		System.err
				.format("Enemy1=%s Enemy2=%s\n deAtacatTotake1=%d  deAtacatToTake2=%d  allied1=%d  allied2=%d\n",
						Boolean.toString(enemy1), Boolean.toString(enemy2),
						deAtacatToTake2, deAtacatToTake1, alliedRegions2,
						alliedRegions1);
		//daca una din superregiuni are inamic si cealalta nu, va avea prioritate mai mica
		if (enemy1 && !enemy2)
			return -1;
		if (enemy2 && !enemy1)
			return 1;
		if (toAttack.getSuperRegion().getArmiesReward() == 0)
			return 1;
		if (o.toAttack.getSuperRegion().getArmiesReward() == 0)
			return -1;
		//se determina prioritatea in functie de numarul de armate
		int toTakeDiff = deAtacatToTake1 - deAtacatToTake2;
		if (toTakeDiff != 0) {
			return deAtacatToTake2 - deAtacatToTake1;
		}
		//daca sunt egale, se alege superregiunea cu mai multe regiuni
		//aliate deja prezente
		if (alliedRegions1 != alliedRegions2)
			return alliedRegions1 - alliedRegions2;
		//in final, se considera superregiunea cu profit mai mare
		return -(toAttack.getSuperRegion().getArmiesReward()
				- o.toAttack.getSuperRegion().getArmiesReward());
	}

	/**
	 * @return forta necesara de adaugat la deploy pentru a cuceri regiunea
	 *         atacata.
	 */
	public int getDeAdaugat() {
		return deAdaugat;
	}

	/**
	 * @return regiunea aliata din care atacam/ in care punem armate.
	 */
	public Region getRepresentative() {
		return representative;
	}

	/**
	 * @param deAdaugat
	 *            forta necesara de adaugat la deploy pentru a cuceri regiunea
	 *            atacata.
	 */
	public void deAdaugat(int deAdaugat) {
		this.deAdaugat = deAdaugat;
	}

	/**
	 * @return numarul minim de armate necesare pentru a cuceri regiunea
	 *         respectiva.
	 */
	public int getTarget() {
		return target;
	}

	/**
	 * @param numarul
	 *            minim de armate necesare pentru a cuceri regiunea respectiva.
	 */
	public void setTarget(int target) {
		this.target = target;
	}

	/**
	 * @return regiunea pe care o atac.
	 */
	public Region getToAttack() {
		return toAttack;
	}

	/**
	 * @param toAttack
	 *            regiunea pe care o atac.
	 */
	public void setToAttack(Region toAttack) {
		this.toAttack = toAttack;
	}

	public void setDeAdaugat(int deAdaugat) {
		this.deAdaugat = deAdaugat;
	}

}
