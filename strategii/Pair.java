package strategii;

import map.Region;
/**
 * 
 * @author #Bugs
 * Clasa Pair cu doua regiuni ce urmeaza sa fie
 * adaugate in coada de prioritati.
 */
public class Pair implements Comparable {

	public Region a;
	public Region b;

	public Pair() {
		super();
	}

	public Pair(Region a, Region b) {
		super();
		this.a = a;
		this.b = b;
	}

	@Override
	public int compareTo(Object o) {	
		//prioritatea va fi data de numarul de armate necesare cuceririi
		Pair other = (Pair) o;
		int rez = this.a.compareTo(other.a);
		if(rez == 0) return (this.b.getArmies() - other.b.getArmies()); 
		return rez;
	}

}
