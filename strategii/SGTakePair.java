package strategii;

import java.util.LinkedList;

import map.SuperRegion;

/**
 * 
 * @author #Bugs
 * Clasa ce contine o pereche de forma
 * <SuperRegiune, cost_luat_intr-o_tura>,
 * pentru a evalua superregiunile si costurile
 * necesare cuceririi lor, cea cu cost minim fiind aleasa.
 */
public class SGTakePair implements Comparable {
	private SuperRegion sg;
	private int cost;
	public LinkedList<Candidate> miscari;
	
	public SGTakePair(SuperRegion sg, int cost, LinkedList<Candidate> miscari) {
		super();
		this.sg = sg;
		this.cost = cost;
		this.miscari = miscari;
	}
	
	public LinkedList<Candidate> getMiscari() {
		return miscari;
	}
	
	public void setMiscari(LinkedList<Candidate> miscari) {
		this.miscari = miscari;
	}
	
	/**
	 * @param sg superregiunea 
	 * @param cost costul total necesar cuceririi sale intr-o tura
	 */
	public SGTakePair(SuperRegion sg, int cost) {
		super();
		this.sg = sg;
		this.cost = cost;
	}
	
	public SuperRegion getSg() {
		return sg;
	}
	
	public void setSg(SuperRegion sg) {
		this.sg = sg;
	}
	
	public int getCost() {
		return cost;
	}
	
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	@Override
	public int compareTo(Object o) {
		//prioritatea va fi data de numarul de armate necesare cuceririi
		return (this.cost - ((SGTakePair)o).cost);
	}
	
}
