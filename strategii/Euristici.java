package strategii;

import java.util.LinkedList;
import java.util.PriorityQueue;

import bot.BotState;
import map.Map;
import map.Region;
import map.SuperRegion;

/**
 * Clasa pentru euristici, calculul superregiunilor si alte lucruri utile.
 * 
 * @author #Bugs
 */
public final class Euristici {

	static int[][] distances;
	static int[][] nextRegion;

	/**
	 * Determina numarul minim de armate necesare pentru a cuceri o regiune
	 * inamica.
	 * 
	 * @param armies
	 *            armatele din regiunea inamica
	 * @param potential
	 *            numarul de armate suplimentare care pot sa ajunga(0 in neutre,
	 *            >0 inamic)
	 * @return numarul minim de armate necesare pentru a cuceri
	 */
	public static int calculateMinimumForce(int armies, int potential) {
		return (int) Math.round((armies) / 0.6);
	}

	/**
	 * Algoritmul Roy-Floyd/Roy-Warshall pentru a determina drumurile minime
	 * intre toate regiunile.
	 * 
	 * @param fullMap
	 *            o lista completa a rxegiunilor si a conexiunilor dintre
	 *            regiuni.
	 */
	public static void roy_floyd(Map fullMap) {
		// calculul tuturor distantelor cu roy floyd/roy warshall in O(n^3)
		// facut o singura data, inainte de alegerea primei regiuni.
		int n = fullMap.getRegions().size();
		// matricea de distante la momentul initial
		int d0[][] = new int[n + 1][];
		// matricea de distante dupa fiecare pas
		int d[][] = null;
		// matricea de muchii pentru drumurile de distanta minima
		int next[][] = new int[n + 1][];

		for (int i = 1; i <= n; i++) {
			d0[i] = new int[n + 1];
			next[i] = new int[n + 1];
			Region r = fullMap.getRegion(i);
			for (int j = 1; j <= n; j++) {
				if (r.isNeighbor(fullMap.getRegion(j))) {
					d0[i][j] = 1; // drumurile nu sunt ponderate, fiecare muchie
									// costa 1
					next[i][j] = j;
				} else if (i != j)
					d0[i][j] = 100000; // i si j nu sunt vecini
			}
		}

		// conform algoritmului roy-floyd, putem incerca pe rand drumuri prin
		// noduri intermediare
		// pentru a determina drumul minim de la un nod la altul.
		for (int k = 1; k <= n; k++) {
			d = new int[n + 1][];
			// matricea la starea curenta, determinata in functie de starea
			// precedenta(D0).
			for (int i = 1; i <= n; i++) {
				d[i] = new int[n + 1];
				for (int j = 1; j <= n; j++) {
					// drumul de la i si j este minimul dintre drumul calculat
					// precedent si drumul via
					// regiunea k
					d[i][j] = min(d0[i][j], d0[i][k] + d0[k][j]);
					if (d0[i][k] + d0[k][j] < d0[i][j]) {
						next[i][j] = next[i][k];
					}
				}
			}
			d0 = d; // se actualizeaza starea precedenta a matricei de drumuri
					// minime
		}

		distances = d;
		nextRegion = next;
	}

	/**
	 * @param regions
	 *            lista regiunilor
	 * @param ID
	 *            id-ul regiunii de start
	 * @return id-ul regiunii urmatoare din drumul minim catre cea mai
	 *         apropriata regiune de pe front.
	 */
	public static int getTransferRegionId(LinkedList<Region> regions, int ID) {
		int closest = -1;
		int minDist = 10000;
		for (Region n : regions) {
			if (!n.isOnFrontier)
				continue;
			int i = n.getId();
			int dist = distances[ID][i];
			if (dist < minDist) {
				minDist = dist;
				closest = n.getId();
			}
		}
		return nextRegion[ID][closest];
	}

	public boolean hasNeutralNeighbours(Region reg, BotState state) {
		for (Region r : reg.getNeighbors())
			return (!r.ownedByPlayer(state.getMyPlayerName()) && !r
					.ownedByPlayer(state.getOpponentPlayerName()));
		return false;
	}

	private static int min(int a, int b) {
		return (a < b) ? a : b;
	}

	/**
	 * Vede daca se poate lua superregiunea respectiva in aceasta tura.
	 * Regiunile care contin wastelands sau sunt marginite/contin regiuni inamice nu sunt considerate.
	 * @param state starea jocului
	 * @param sg superregiunea analizata
	 * @return o lista de miscari daca este posibila cucerirea superregiunii sg, null altfel.
	 */
	public static LinkedList<Candidate> takeSuperregion(BotState state,
			SuperRegion sg) {
		LinkedList<Candidate> solution = null;

		// la inceput, setam forta efectiva == numarul de armate
		// daca o regiune este folosita intr-un atac, nu mai putem spune ca are
		// numarul initial de armate
		for (Region r : state.getVisibleMap().getMyRegions())
			r.setActualForce(r.getArmies());
		for (Region r : sg.getSubRegions()) {
			if (r.ownedByPlayer(state.getOpponentPlayerName()))
				return null; // superregiunea are un inamic;
			if (r.ownedByPlayer(state.getMyPlayerName()))
				continue;
			Region vecinCareAtaca = null;
			int deAdaugatMin = 0;
			int minimum = Integer.MAX_VALUE;
			int fortaNecesara = Euristici.calculateMinimumForce(r.getArmies(),
					0);
			LinkedList<Region> vecini = r.getNeighbors();
			for (Region vecin : vecini) {
				//if (vecin.ownedByPlayer(state.getOpponentPlayerName()))
					//return null; // nu are sens
				if (!vecin.ownedByPlayer(state.getMyPlayerName()))
					continue;
				// vecin e regiunea mea, r e regiune neutra de atacat
				int deAdaugat = fortaNecesara - vecin.getActualForce() + 1;
				if(deAdaugat < minimum) {
					minimum = deAdaugat;
					deAdaugatMin = deAdaugat;
					vecinCareAtaca = vecin;
				}
			}
			if(minimum == Integer.MAX_VALUE) return null;
			if (solution == null)
				solution = new LinkedList<Candidate>();
			//adaugam la solutie miscarile necesare
			solution.add(new Candidate(vecinCareAtaca, deAdaugatMin, fortaNecesara, r, state));
			//seteaza forta efectiva a regiunii din care se va ataca, scazand numarul de unitati
			//folosite in atac
			if(deAdaugatMin > 0) { 
				vecinCareAtaca.setActualForce(vecinCareAtaca.getActualForce() +
					deAdaugatMin - fortaNecesara);
			} else vecinCareAtaca.setActualForce(1);
		}
		return solution;
	}

}
